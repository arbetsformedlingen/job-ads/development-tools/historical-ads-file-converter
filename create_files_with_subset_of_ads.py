#  -*- coding: utf-8 -*-
import sys
import random
import logging

import settings
from file_handler import read_jsonl, write_jsonl

logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)

percent = 1


def select_random_ads(all_ads, percent):
    percentage = int(percent) / 100
    desired_number = int(len(all_ads) * percentage) + 1
    random_ads = random.sample(all_ads, desired_number)
    log.info(f"Selected {len(random_ads)} ads, {percent} % of all ads")
    return random_ads


def convert_files(file_names):
    log.info(f"Started conversion of {file_names}")
    output_file_names = []
    for file_name in file_names:
        log.info(f"Loading file {file_name} to be reduced to {percent} % of original")
        list_of_ads_from_file = read_jsonl(file_name)
        log.info(f"{len(list_of_ads_from_file)} ads loaded")
        random_ads = select_random_ads(list_of_ads_from_file, percent)
        output_filename = f"{file_name.removesuffix('.jsonl')}_{percent}_percent.jsonl"
        write_jsonl(random_ads, output_filename)
        output_file_names.append(output_filename)
        log.info(f"Finished reducing {output_filename}")
    return output_file_names


if __name__ == '__main__':
    """
    jsonl to jsonl with a smaller number of ads selected randomly
    Parameters:  filename n
    where n is how many percent of the original should be included (default 1%)
    
    """
    convert_files(file_names=sys.argv[1:])
