import glob
import json_lines_to_metadata
from compress import compress_files
import create_files_with_subset_of_ads

import logging
import settings

logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)


def run_conversion():
    """
    * Processes all files matching name pattern "2*.jsonl" (e.g "2019.jsonl")
    * convert jsonl to reduced (1% of the ads) jsonl files
    * convert jsonl to metadata jsonl files
    * convert jsonl to reduced jsonl files (1% af the ads selectedf randomly
    * zip jsonl files
    * zip reduced jsonl files
    * zip metadata jsonl files
    """
    list_of_jsonl_files = glob.glob("2*.jsonl")
    log.info(f"Started conversion of {list_of_jsonl_files}")
    metadata_file_names = json_lines_to_metadata.run_conversion(list_of_jsonl_files)
    reduced_file_names = create_files_with_subset_of_ads.convert_files(list_of_jsonl_files)
    log.info("All conversions completed")

    # zip all files
    compress_files(list_of_jsonl_files)
    compress_files(metadata_file_names)
    compress_files(reduced_file_names)
    log.info(f"File compression completed")


if __name__ == '__main__':
    """
    Usage:
    python do_all_conversions.py 
    
    """
    run_conversion()
