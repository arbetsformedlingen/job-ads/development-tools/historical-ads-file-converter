import logging
import sys

import pandas
import settings

logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)


def convert_single_file(file_name):
    csv_file_name = f"{file_name.removesuffix('jsonl')}csv"
    log.info(f"starting conversion of {file_name} to {csv_file_name}")
    try:
        df = pandas.read_json(file_name, lines=True)
        log.info(f"Loaded ads from file")
        df.to_csv(csv_file_name, index=None, encoding="utf-8")
    except Exception as e:  # So that a failed conversion of one file doesn't stop a batch job with multiple files
        log.error(f"Conversion of {file_name} failed: {e}")
    log.info(f"Conversion of {file_name} to csv successful. Output csv file: {csv_file_name}")
    return csv_file_name


def run_conversion(json_lines_file_names):
    csv_file_names = []
    for file_name in json_lines_file_names:
        csv_file_name =convert_single_file(file_name)
        csv_file_names.append(csv_file_name)
    return csv_file_names




if __name__ == '__main__':
    """
    Usage:
    python jsonlines_to_csv.py file1.jsonl file2.jsonl
    
    output:
    file1.csv, file2.csv
    
    """
    run_conversion(sys.argv[1:])
