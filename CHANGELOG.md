Changelog
===============================
# 2022-02-04

* Added more cleanup of files
* Added language detection (until it's done in jobsearch-importers)
* New file handling for better performance
* 

# 2022-01-20

* conversion of chunks from the importer to jsonlines, 
* conversion of jsonlines to csv, metadata and jsonline with reduced number of ads

