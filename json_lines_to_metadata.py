import sys
import json
import logging

from file_handler import read_jsonl, write_jsonl

logging.basicConfig(level="INFO", datefmt='%Y-%m-%d %H:%M:%S',
                    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s')
log = logging.getLogger(__name__)


def open_jsonlines_file(filename):
    data = []
    with open(filename, encoding='utf-8') as f:
        for line in f:
            try:
                data.append(json.loads(line))
            except Exception as e:
                log.error(f"Error when reading ad from file {filename}: {e}, ignoring it ")
    return data


def remove_unwanted_fields(ad):
    for key in ['headline', 'description', 'employer', 'application_details', 'hash', 'id', 'external_id', 'uuid',
                'logo_url', 'webpage_url']:
        ad.pop(key, None)
    return ad


def process_all_ads(all_ads):
    for ad in all_ads:
        remove_unwanted_fields(ad)
    log.info(f"{len(all_ads)} ads processed")
    return all_ads


def run_conversion(input_files):
    log.info(f"run conversion with files: {input_files}")
    file_names = []
    for f in input_files:
        data = read_jsonl(f)  # sys.argv[1]
        processed_ads = process_all_ads(data)
        output_file_name = f"{f.removesuffix('.jsonl')}_metadata.jsonl"
        write_jsonl(processed_ads, output_file_name)
        file_names.append(output_file_name)
        log.info(f"Converted {f} to {output_file_name}")
    log.info(f"Conversion of {len(input_files)} file(s) completed")
    return file_names


if __name__ == '__main__':
    """
    Usage:
    
    """

    run_conversion(sys.argv[1:])
