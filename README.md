# Code for converting historical ad files to different format

### Possible conversions:
* Files converted in importer to json lines files (.jsonl)
* jsonl to csv
* jsonl to reduced files, where some fileds (e.g. description, id, headline) have been removed.
* compression (zip) of any file.

### Todo: 
* uploader to AWS S3

