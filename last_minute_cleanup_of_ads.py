import logging

import settings
import language_detection

logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)


def remove_microseconds_from_timestamp(ad):
    for key_name in ['application_deadline', 'publication_date', 'last_publication_date', 'removed_date']:
        if value := ad.get(key_name, None):
            if len(value) > 20:
                ad[key_name] = value[0:19]  # to remove microseconds that are in some dates


def cleanup_ad(ad):
    """
    Removal of unwanted data, conversion if needed
    Things that should have been done in jobsearch-importer conversion
    but are done here so that we don't have to redo the time-consuming
    enrichment and conversion
    """
    language_detection.detect_language_in_ad(ad)

    remove_microseconds_from_timestamp(ad)

    # sorting to minimise problems when conversion to other formats
    ad = dict(sorted(ad.items()))

    # This field has personal data
    ad['application_contacts'] = []

    # The 'other' field is often used for emails and names
    if other := ad.get("application_details", {}).get('other', ' '):
        if '@' in other:
            ad["application_details"]["other"] = None
            log.warning(f"removed suspected email: {other}")
