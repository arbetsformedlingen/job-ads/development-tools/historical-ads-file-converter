import logging
from langdetect import detect
from langdetect import DetectorFactory

logging.basicConfig()
logging.getLogger(__name__).setLevel(logging.INFO)
log = logging.getLogger(__name__)

"""
Language detection algorithm is non-deterministic, which means that if you try to run it on a text which is either
 too short or too ambiguous, you might get different results everytime you run it.
To enforce consistent results, call DetectorFactory.seed = 0  before the first language detection:
"""
DetectorFactory.seed = 0


def process_ads(data):
    for ad in data:
        detect_language_in_ad(ad)
    return data


def detect_language_in_ad(ad):
    text_from_ad = f"{ad['headline']} {ad['description']['text']}"
    text_for_detection = _format_text_for_detection(text_from_ad)
    if text_for_detection:
        ad['detected_language'] = _detect_langcode(text_for_detection)
    else:
        ad['detected_language'] = None
    return ad


def _format_text_for_detection(text_for_detection):
    if not text_for_detection:
        log.warning(f"No text to use for detection")
        return None

    elif len(text_for_detection) < 50:
        # Short texts give unreliable results, require a minimum of 50 chars
        return None

    text_for_detection = text_for_detection.strip()
    if len(text_for_detection) > 400:
        # shorten text for performance reasons
        text_for_detection = text_for_detection[:400]

    # Note: If text to detect is in uppercase, the langdetect function might predict
    # the language to be German.
    text_for_detection = text_for_detection.lower()
    return text_for_detection


def _detect_langcode(text_for_detection):
    """
    This function will return language code in ISO 639-1 format
    if it is any of the languages langdetect is capable of detecting
    https://pypi.org/project/langdetect/
    """
    try:
        return detect(text_for_detection)
    except Exception as e:
        log.error(f'Error when trying to detect language in: {text_for_detection} {e}')
        return None
