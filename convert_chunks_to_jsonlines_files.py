import sys
import json
import logging
import glob

import settings
import last_minute_cleanup_of_ads

logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)


def json_to_jsonlines(output_filename):
    """
    Takes smaller files from conversion ("historical-yyyy.n.json")
    and saves them into a single jsonlines file
    the ad dictionaries are sorted to avoid errors when converting to csv
    """
    input_filenames = glob.glob("historical-20*.json")
    log.info(f"files to load: {input_filenames}")
    total = 0
    with open(output_filename, "w", encoding='utf-8') as outfile:
        log.info(f"opening output file {output_filename}")
        for infile_name in input_filenames:
            with open(infile_name) as infile:
                row_list = json.loads(infile.read())
                log.info(f"loading {len(row_list)} ads from {infile_name}")
                for row in row_list:
                    sorted_row = dict(sorted(row.items()))

                    # TODO make sure this is updated
                    last_minute_cleanup_of_ads.cleanup_ad(sorted_row)

                    json.dump(sorted_row, outfile, ensure_ascii=False)
                    outfile.write('\n')
                total += len(row_list)

    log.info(f"saved {total} ads from {len(input_filenames)} chunk files to {output_filename}")


if __name__ == '__main__':
    """
    Usage
    This program will take all files in the current directory matching the file name pattern 'historical-*.json' 
    and convert them to a single jsonl file
    python convert_chunks_to_jsonlines_files.py outputfile.jsonl
    """
    json_to_jsonlines(output_filename=sys.argv[1])
