import sys
from zipfile import ZipFile, ZIP_DEFLATED
import logging
import settings

logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)


def compress_files(files_to_compress):
    log.info(f"Files to compress: {files_to_compress}")
    for f in files_to_compress:
        compress_one_file(f)
    log.info("All files compressed")


def compress_one_file(file_to_compress):
    compressed_file_name = f"{file_to_compress.removesuffix('.jsonl')}_jsonl.zip"
    with ZipFile(compressed_file_name, 'w', compression=ZIP_DEFLATED, compresslevel=1) as zf:
        zf.write(file_to_compress)
    log.info(f"Compressed '{file_to_compress}' to '{compressed_file_name}'")



if __name__ == '__main__':
    """
    Usage:
    python compress.py file1.jsonl file2.jsonl file3.jsonl
    
    Output, compressed files with .zip extension:
    file1.jsonl.zip file2.jsonl.zip file3.jsonl.zip
    """
    compress_files(sys.argv[1:])
