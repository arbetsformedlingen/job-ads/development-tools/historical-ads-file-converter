import pickle
import logging

import settings
logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)


def pickle_something(file_name, pickle_this):
    with open(file_name, 'wb') as f:
        log.info(f"Pickle to {file_name}")
        pickle.dump(pickle_this, f, protocol=pickle.HIGHEST_PROTOCOL)


def unpickle(file_name):
    log.info(f"Unpickle from {file_name}")
    with open(file_name, 'rb') as f:
        return pickle.load(f)
