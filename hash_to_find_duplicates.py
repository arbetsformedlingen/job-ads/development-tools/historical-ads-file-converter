import logging
import glob
import hashlib

import settings
import file_handler

import pickling
logging.basicConfig(level="INFO", datefmt=settings.log_datefmt, format=settings.log_format)
log = logging.getLogger(__name__)



def hash_str(text_to_hash):
    hash_object = hashlib.sha1(text_to_hash.encode("utf-8"))
    hash_string = hash_object.hexdigest()
    return hash_string

if __name__ == '__main__':
    short_ads = []
    broken_ads = []
    list_of_jsonl_files = glob.glob('2016-2021\\*beta1.jsonl')
    log.info(f"Started conversion of {list_of_jsonl_files}")

    with open('hashes_new/new_hashes_3.txt', 'w') as hash_file:
        for f in list_of_jsonl_files:

            ad_list = file_handler.read_jsonl(f)
            for ad in ad_list:
                headline = ad.get('headline', ' ')
                if description := ad.get('description', {}).get('text', ' '):
                    text_to_hash = f"{headline} {description}"
                else:
                    text_to_hash = f"{headline}"
                    broken_ads.append(ad)
                sha1_hash = hash_str(text_to_hash)
                hash_file.write(f"{sha1_hash}\n")

                if len(text_to_hash) < 20:
                    short_ads.append((ad, sha1_hash))
    log.info(f"{len(short_ads)} short texts")

    pickling.pickle_something("hashes_new/short_ads.pkl", short_ads)
    pickling.pickle_something("hashes_new/broken_ads.pkl", broken_ads)

    log.info(f"Complete {list_of_jsonl_files}")


